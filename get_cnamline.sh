DIR=/pstar/tdr
FILEDIR=dfkbin
FILE=cnamline_cnt.sh
if
  test "$1" = ""
then
  DT=`date -u +%Y%m%d%H%M | cut -c1-11`
  case $DT in
     *0)
        LAST10=`expr $DT \- 5`
        ;;
     *)
        LAST10=`expr $DT \- 1`
        ;;
  esac
else
  LAST10=$1
fi
#LAST10=2019090805
date +%s
$DIR/$FILEDIR/$FILE $LAST10
date +%s
tar -czvf /tmp/dal_acc_accuname_report_${LAST10}.tar.gz /tmp/${LAST10}.newcntfile  
rm /tmp/${LAST10}.newcntfile
#send it somewhere
