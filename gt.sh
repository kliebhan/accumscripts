date +%s
FOOUTFILE=/tmp/testdata.txt.gz
TMOOUTFILE=/tmp/TMOdata.txt.gz
echo "" | gzip > $FOOUTFILE
YR=2018
MTH=10
DY=23
HR=17
grep -e :DELEGATOR: $HOME/dfkbin/edge_ip_list | grep -v "^#" | grep -v IRVSPC62 > /tmp/$$.SERVERS
for MN in `echo 00 01 02 03 04 05 06 07 08 09`
#for MN in `echo 02 03`
do
  for SERVER in `cat /tmp/$$.SERVERS`
  do
    date +%s
    SVR=`echo $SERVER | cut -f4 -d":"`
    echo doing $SVR minute $MN
    TDRIP=`echo $SERVER | cut -f5 -d":"`
    MINFILE=/tmp/$$.$SVR$MN.file
    FND=0
    while
      test $FND -eq 0
    do
      if
        test -r /home/tdr/delegator/archive/tdrmon@${TDRIP}/${YR}${MTH}${DY}${HR}${MN}*.gz 
      then
        echo found $SVR $MN
        FND=1
        zcat /home/tdr/delegator/archive/tdrmon@${TDRIP}/${YR}${MTH}${DY}${HR}${MN}*.gz | \
         cut -f1,5,8,17,20 -d"|" | \
         # take out lines with alphas in A number (column 2)
         grep -E -v '^([^\|]*\|){1}[a-zA-Z][^\|]*' | \
         # remove lines with special character n A nums like dots in IP addresses
         #grep -v '\.' | \
         grep -E -v '^*\|.+\..+\|.+\|.+' | \
         # make last two B number digts "n"
         sed -e "s/^\(.*\)|\(.*\)|\(.*\)..|\(.*\)|\(.*$\)$/\1|\2|\3nn|\4|\5/" | \
         # add category 101 to NoProfile
         sed -e "s/^\(.*\)|\(.*\)|\(.*\)|-1|NoProfile$/\1|\2|\3|101/" | \
         # add category 101 to ProfileInactive
         sed -e "s/^\(.*\)|\(.*\)|\(.*\)|-1|ProfileInactive$/\1|\2|\3|101/" | \
         # add category 101 to Forbidden
         sed -e "s/^\(.*\)|\(.*\)|\(.*\)|-1|Forbidden$/\1|\2|\3|101/" | \
         # add category 911 to AnonAction but not expecting these due to alpha removal
         sed -e "s/^\(.*\)|\(.*\)|\(.*\)|-1|AnonAction$/\1|\2|\3|9111/" | \
         # FO gets first 4 cloumns
         cut -f1-4 -d"|" >> $MINFILE
      else
        echo "$SVR $MN not found"
        FND=2
      fi
    done
    if
      test $FND -eq 1
    then
      gzip -c $MINFILE >> $FOOUTFILE
      #gzip $MINFILE
      rm $MINFILE
    fi
  done
done
echo "FO file complete"
echo "building TMO file"
# TMO gets first 3 cloumns
zcat $FOOUTFILE | cut -f1-2 -d"|" | gzip > $TMOOUTFILE
echo "checking line count and format"
echo "FO file:"
zcat $FOOUTFILE | wc -l
zcat $FOOUTFILE | head -5
echo "TMO file:"
zcat $TMOOUTFILE | wc -l
zcat $TMOOUTFILE | head -5
rm -f /tmp/$$.*
#date +%s
