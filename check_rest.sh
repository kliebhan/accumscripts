LOG=$HOME/logs/reststatus.log
EMAILDIR=/pstar/tmoreports/outbound/enum_heartbeat_failure
FC=0
grep :REST: $HOME/dfkbin/edge_ip_list | grep -v "^#" > /tmp/$$.SERVERS
SC=`cat /tmp/$$.SERVERS | wc -l`
for SERVER in `cat /tmp/$$.SERVERS`
do
  PROTIP=`echo $SERVER | cut -f2 -d":"`
  NAME=`echo $SERVER | cut -f4 -d":"`
  R=`curl -ikSs -X GET -H "Content-Type: application/xml" -H "Accept: application/xml" http://${PROTIP}:8080/firstorion.com/callScreening/v1/healthCheck | grep "204 No" | wc -l`
  if 
    test "$R" != "1"
  then
    D=`date -u +%s`
    echo $D:$NAME:RESTfailure >> $LOG
    FC=`expr $FC \+ 1`
  fi
done
if
  test $FC -eq 0
then
  D=`date -u +%s`
  echo $D:$SC:RESTPASSED >> $LOG
fi
rm -f /tmp/$$.*
