YR=2018
MTH=10
DY=23
HR=17
grep -e :DELEGATOR: $HOME/dfkbin/edge_ip_list | grep -v "^#" | grep -v IRVSPC62 > /tmp/$$.SERVERS
for MN in `echo 09`
do
  for SERVER in `cat /tmp/$$.SERVERS`
  do
    SVR=`echo $SERVER | cut -f4 -d":"`
    echo doing $SVR minute $MN
    TDRIP=`echo $SERVER | cut -f5 -d":"`
    MINFILE=/tmp/$$.$SVR$MN.file
    FND=0
    while
      test $FND -eq 0
    do
      if
        test -r /home/tdr/delegator/archive/tdrmon@${TDRIP}/${YR}${MTH}${DY}${HR}${MN}*.gz 
      then
        echo found $SVR $MN
        FND=1
      else
        echo "$SVR $MN not found"
        FND=2
      fi
    done
  done
done
rm -f /tmp/$$.*
