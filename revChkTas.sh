OUTFILE=/tmp/revChkTas.out
CNTFILE=/tmp/$$.rc
TASFILE=/tmp/TASNAMES
#OUTFILE=./revChkTas.out
#CNTFILE=./1793.rc
#TASFILE=./TASNAMES
SKIP=0
if
  test $SKIP -ne 1
then
rm -f $CNTFILE
EPOCH=`date +%s`
#M45=`echo "$EPOCH - 2700" | bc`
M30=`echo "$EPOCH - 1800" | bc`
MN30=`date -u -d@${M30} +%M` 
HR30=`date -u -d@${M30} +%H` 
MN15=`echo "$MN30 / 15 * 15" | bc`
DT30=`date -u -d@${M30} +%Y%m%d`
STAMP=${DT30}T${HR30}${MN15}
for SERVER in `echo DLLSPC02 DLLSPC03 ELGSPC02 ELGSPC03 NSVSPC01 NSVSPC02`
do
  #echo /home/tdr/tpim/$SERVER/*${SERVER}.${STAMP}*.log.gz
  zgrep -i httpresponsecount /home/tdr/tpim/$SERVER/*${SERVER}.${STAMP}*.log.gz >> $CNTFILE
done
fi
date +%s > $OUTFILE
if
  test -f $CNTFILE
then
  for TAS in `cat $TASFILE`
  do
    TASCNT=`grep $TAS $CNTFILE | cut -f6 -d"|" | paste -sd+ - | bc`
    echo $TAS:$TASCNT >> $OUTFILE
  done
else
  echo no file
fi
#rm -f $CNTFILE
