MASTER_EDGELIST=/pstar/tdr/dfkbin/edge_ip_list
MASTER_ENUMFILE=/pstar/tdr/dfkbin/enum2chk_list
rm -f $MASTER_ENUMFILE
for ENUM in `grep -v "^#" $MASTER_EDGELIST | grep :ENUM:`
do
  echo $ENUM >> $MASTER_ENUMFILE
done
ENUMSERVER_CNT=`cat $MASTER_ENUMFILE | wc -l`
echo ${ENUMSERVER_CNT}:$MASTER_ENUMFILE
