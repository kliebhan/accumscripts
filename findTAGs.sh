INFILE=minusscam.txt
#for DY in `echo 04`
for DY in `echo 04 05 06 07 08 09 10 11 12 13`
do
#for HR in `echo 19 20 21 22 23`
for HR in `echo 00 01 02 03 04 05 06 07 08 09 10 11 12 13 14 15 16 17 18 19 20 21 22 23`
#for HR in `echo 23`
do
  #TS=${1}${HR}
  TS=201804${DY}${HR}
  OUTFILE=/tmp/cnt${TS}.data
  rm -f $OUTFILE
  #echo $TS
  #if
  #  test $2 = ""
  #then
    TMPFILE=/tmp/$$.${HR}
    rm -f $TMPFILE
    for SERVER in `grep :ENUM: $HOME/dfkbin/edge_ip_list | grep -v "^#"`
    do
      TDRIP=`echo $SERVER | cut -f5 -d":"`
      SVR=`echo $SERVER | cut -f4 -d":"`
      #echo ${SVR}
      zgrep \|999999\| $HOME/archive/tdrmon@${TDRIP}/${TS}*.gz | cut -f5 -d"|" >> $TMPFILE
    done
  #else
  #  TMPFILE=/tmp/$2.${HR}
  #fi
  for NUM in `cat $INFILE`
  do
    CNT=`grep -c ${NUM} $TMPFILE`
    echo ${NUM}:${CNT} >> $OUTFILE
  done
  grep -v 0$ $OUTFILE > damage/gotdels${TS}
  rm -f $TMPFILE $OUTFILE
done
done
