LOG=$HOME/logs/enumstatus.log
EMAILFILE=/tmp/$$.email
EMAILDIR=/pstar/tmoreports/outbound/enum_heartbeat_failure
D=`date -u +%s`
LD=`tail -1 $LOG | cut -f1 -d":"`
DIFF=`echo "$D - $LD" | bc`
if
  test $DIFF -gt 60
then
  echo "ENUM Heartbeats have stopped - check the DALLAS ACC and crontab" > $EMAILFILE
else
  LASTREC=`tail -1 $LOG | grep -c PASSED`
  if
    test $LASTREC -eq 0
  then
    LASTFAIL=`tail -1 $LOG | cut -f2 -d":"`
    echo "Server $LASTFAIL and maybe others are failing heartbeats, check $LOG for info" > $EMAILFILE
  fi
fi
if
  test -f $EMAILFILE
then
  mv $EMAILFILE $EMAILDIR/"ENUM HEARTBEAT ERROR $D"
fi
rm -f /tmp/$$.*
