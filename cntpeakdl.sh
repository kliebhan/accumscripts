#OUTFILE=$HOME/logs/pk_server_cnts.txt
OUTFILE=/tpim_bind_mount/pk_server_cnts.txt
#OUTFILE2=/tpim_bind_mount/pk_spam_cnts.txt
if
  test ! -f $OUTFILE
then
    echo TPS:PKMNCNT:HR:DY:MTH:SRV:TYPE:PKMN > $OUTFILE
fi
TS=`date +%s`
#echo $TS
TS=`echo $TS - 180 | bc`
#TS=`echo $TS - 3600 | bc`
#echo $TS
#DT=`date -d@${TS} -u +%Y%m%d%H%M`
#echo $DT
#date -u +%Y%m%d%H%M
DT=`date -d@${TS} -u +%Y%m%d%H`
MTH=`date -d@${TS} -u +%m`
DY=`date -d@${TS} -u +%d`
HR=`date -d@${TS} -u +%H`
if
  test "$1" != ""
then
  HR=$1
  DT=`date -d@${TS} -u +%Y%m%d`$HR
else
  DT=`date -d@${TS} -u +%Y%m%d%H`
fi
#TMP=`date -d@${TS} -u +%Y%m%d`
#DT=${TMP}15
#echo $DT
#for ST in `echo DELEGATOR REST CNAM`
for ST in `echo DELEGATOR`
do
  grep :${ST}: $HOME/dfkbin/edge_ip_list | grep -v "^#" > /tmp/$$.SERVERS
  if
    test "${ST}" = "CNAM"
  then
    INC=5
    DIV=300
  else
    INC=1
    DIV=60
  fi
  for SERVER in `cat /tmp/$$.SERVERS`
  do
    SRV=`echo $SERVER | cut -f4 -d":"`
    TDRIP=`echo $SERVER | cut -f5 -d":"`
    CNTFILE=/tmp/$$.hrcnt_$SRV
    MN=00
    while
      test $MN -lt 60
    do
      if
        test "$ST" = "DELEGATOR"
      then
        CNT=`zgrep \|DELEGATOR\| $HOME/archive/tdrmon@${TDRIP}/${DT}${MN}*.gz | wc -l`
      else
        CNT=`zcat $HOME/archive/tdrmon@${TDRIP}/${DT}${MN}*.gz | wc -l`
      fi
      echo "$CNT:$MN" >> $CNTFILE
      MN=`expr $MN \+ ${INC}`
      Z=`echo $MN | wc -c`
      if
        test $Z -eq 2
      then
        MN=0${MN}
      fi
    done
    if
      test ! -f $CNTFILE
    then
      echo 0:0:$HR:$DY:$MTH:$SRV:$ST:NA >> $OUTFILE
    else
      PKCNT=`sort -n $CNTFILE | tail -1`
      PKMNCNT=`echo $PKCNT | cut -f1 -d":"`
      PKMN=`echo $PKCNT | cut -f2 -d":"`
      TPS=`echo "scale=2; $PKMNCNT / ${DIV}" | bc -l`
      #echo $TPS:$PKMNCNT:$HR:$DY:$MTH:$SRV:$ST:$PKMN
      echo $TPS:$PKMNCNT:$HR:$DY:$MTH:$SRV:$ST:$PKMN >> $OUTFILE
    fi
  done
  rm -f $CNTFILE
done
rm -f /tmp/$$.*
