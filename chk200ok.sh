INFILE=/tmp/4xx.txt
INFILE=/tmp/22474.4xxnums
cp $INFILE /tmp/$$.4xxnums
for SERVER in `grep :REST: $HOME/dfkbin/edge_ip_list | grep -v "^#"`
do
  TDRIP=`echo $SERVER | cut -f5 -d":"`
  SVR=`echo $SERVER | cut -f4 -d":"`
  #for HR in `echo 00 01 02 03 04 05 06 07 08 09 10 11 12 13 14 15 16 17 18 19 20 21 22 23`
  #do
    TMPFILE=/tmp/$$.${SVR}
    echo ${SVR}
    zgrep \|200-OK\| $HOME/archive/tdrmon@${TDRIP}/${1}*.gz | cut -f8 -d"|" | sort -u > $TMPFILE
    #echo 12012047131 >> $TMPFILE
    TOT=0
    LINE=0
    STILL=0
    for NUM in `cat /tmp/$$.4xxnums`
    do
      LINE=`expr $LINE \+ 1`
      CNT=`grep -c ${NUM} $TMPFILE`
      if
        test $CNT -gt 0
      then
        TOT=`expr $TOT \+ 1`
      else
        STILL=`expr $STILL \+ 1`
        #echo "did NOT find ${NUM} queued for next hour"
        echo ${NUM} >> /tmp/$$.still4xx
      fi
      echo "$LINE:$TOT:$STILL"
    done
    rm -f $TMPFILE
    mv /tmp/$$.still4xx /tmp/$$.4xxnums
  #done
done
echo "number still 4xx in /tmp/$$.4xxnums"
wc -l /tmp/$$.4xxnums
