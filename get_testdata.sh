date +%s
OUTFILE=/tmp/testdata.txt.gz
echo "" | gzip > $OUTFILE
YR=2018
MTH=10
DY=17
HR=17
grep -e :DELEGATOR: $HOME/dfkbin/edge_ip_list | grep -v "^#" | grep -v IRVSPC62 > /tmp/$$.SERVERS
#for MN in `echo 00 01 02 03 04 05 06 07 08 09`
for MN in `echo 02 03`
do
  for SERVER in `cat /tmp/$$.SERVERS`
  do
    date +%s
    SVR=`echo $SERVER | cut -f4 -d":"`
    echo doing $SVR minute $MN
    TDRIP=`echo $SERVER | cut -f5 -d":"`
    MINFILE=/tmp/$$.$SVR$MN.file
    FND=0
    while
      test $FND -eq 0
    do
      if
        test -r /home/tdr/delegator/archive/tdrmon@${TDRIP}/${YR}${MTH}${DY}${HR}${MN}*.gz 
      then
        echo found $SVR $MN
        FND=1
        zcat /home/tdr/delegator/archive/tdrmon@${TDRIP}/${YR}${MTH}${DY}${HR}${MN}*.gz | cut -f1,5,8,17 -d"|" | sed -e "s/^\(.*\)|\(.*\)|\(.*\)..|\(.*$\)$/\1|\2|\3nn|\4/" >> $MINFILE
      else
        sleep 5
      fi
    done
    gzip -c $MINFILE >> $OUTFILE
    #gzip $MINFILE
    rm $MINFILE
  done
done
rm -f /tmp/$$.*
date +%s
