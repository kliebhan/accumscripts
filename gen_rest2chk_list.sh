MASTER_EDGELIST=/pstar/tdr/dfkbin/edge_ip_list
MASTER_RESTFILE=/pstar/tdr/dfkbin/rest2chk_list
rm -f $MASTER_RESTFILE
for REST in `grep -v "^#" $MASTER_EDGELIST | grep :REST:`
do
  echo $REST >> $MASTER_RESTFILE
done
RESTSERVER_CNT=`cat $MASTER_RESTFILE | wc -l`
echo ${RESTSERVER_CNT}:$MASTER_RESTFILE
