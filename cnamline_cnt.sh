rm -f /tmp/${1}.newcntfile
DIR=/pstar/tdr
EDGEFILE=$DIR/dfkbin/edge_ip_list
SKIPCNAM=0
SKIPREST=0
SKIPFNFP=0
if
  test "$SKIPCNAM" != "1"
then

grep :CNAM: $EDGEFILE | grep -v "^#" > /tmp/$$.SERVERS
for SERVER in `cat /tmp/$$.SERVERS`
do
  TDRIP=`echo $SERVER | cut -f5 -d":"`
  SERVERNAME=`echo $SERVER | cut -f4 -d":"`
  zgrep -v HTTP $DIR/archive/tdrmon@${TDRIP}/${1}*.gz | cut -f5-8 -d"|" >> /tmp/$$.cnam_req
  cat /tmp/$$.cnam_req | wc -l >> /tmp/$$.cnam_cnt
  cut -f3 -d"|" /tmp/$$.cnam_req | grep -c "." >> /tmp/$$.cnamtag_cnt
done
#wc -l /tmp/$$.cnam_req
cut -f1 -d"|" /tmp/$$.cnam_req | sort | uniq -c > /tmp/$$.cnam_num_cnt
cut -c1-7 /tmp/$$.cnam_num_cnt | sed -e "s/ //g" > /tmp/$$.cnts
cut -c9- /tmp/$$.cnam_num_cnt > /tmp/$$.nums
paste -d"|" /tmp/$$.nums /tmp/$$.cnts > /tmp/$$.newfile
sort /tmp/$$.newfile > /tmp/$$.sortfile
cut -f1 -d"|" /tmp/$$.sortfile > /tmp/$$.sortnums
cut -f2 -d"|" /tmp/$$.sortfile > /tmp/$$.sortcnt
grep \|\|$ /tmp/$$.cnam_req | cut -f1 -d"|" | sort -u > /tmp/$$.nonamenums
diff /tmp/$$.sortnums /tmp/$$.nonamenums | grep "^<" | cut -c3- > /tmp/$$.havename
cat /tmp/$$.nonamenums | sed -e "s/^/${1}|/" | sed -e "s/$/|1|False|Nil/" > /tmp/$$.False
cat /tmp/$$.havename | sed -e "s/^/${1}|/" | sed -e "s/$/|1|True|Nil/" > /tmp/$$.True
cat /tmp/$$.False /tmp/$$.True | sort > /tmp/$$.FT
paste -d"|" /tmp/$$.FT /tmp/$$.sortcnt > /tmp/${1}.newcntfile
#grep 1\|True /tmp/${1}.newcntfile | cut -f6 -d"|" | paste -sd+ - | bc
#grep 1\|False /tmp/${1}.newcntfile | cut -f6 -d"|" | paste -sd+ - | bc
#CCNT=`cat /tmp/$$.cnam_cnt | paste -sd+ - | bc`
#CTAGCNT=`cat /tmp/$$.cnamtag_cnt | paste -sd+ - | bc`
#echo "$1:$CCNT:TOTCNAM"
#echo "$1:$CTAGCNT:TAGCNAM"
rm -f /tmp/$$.*
fi
#
#
#REST
if
  test "$SKIPREST" != "1"
then

grep -e :REST: $EDGEFILE | grep -v "^#" > /tmp/$$.SERVERS
for SERVER in `cat /tmp/$$.SERVERS`
do
  TDRIP=`echo $SERVER | cut -f5 -d":"`
  zgrep \|SpamRule\| $DIR/archive/tdrmon@${TDRIP}/${1}*.gz >> /tmp/$$.onnetspam
  #zgrep -c \|SpamRule\| $DIR/archive/tdrmon@${TDRIP}/${1}*.gz | cut -f2 -d":" >> /tmp/$$.rcnt
  #zgrep -c POST $DIR/archive/tdrmon@${TDRIP}/${1}*.gz | cut -f2 -d":" >> /tmp/$$.rtcnt
done
#wc -l /tmp/$$.onnetspam
BKT=0
while 
  test $BKT -lt 16
do
  grep SpamRule\|$BKT\| /tmp/$$.onnetspam | cut -f5 -d"|" | sort | uniq -c > /tmp/$$.fn${BKT}
  BKTCNT=`cat /tmp/$$.fn${BKT} | wc -l`
  if
    test $BKTCNT -gt 0
  then
    cut -c1-7 /tmp/$$.fn${BKT} | sed -e "s/ //g" > /tmp/$$.cnts
    cut -c9- /tmp/$$.fn${BKT} > /tmp/$$.nums
    paste -d"|" /tmp/$$.nums /tmp/$$.cnts > /tmp/$$.newfile
    sort /tmp/$$.newfile > /tmp/$$.sortfile
    cut -f1 -d"|" /tmp/$$.sortfile > /tmp/$$.sortnums
    cut -f2 -d"|" /tmp/$$.sortfile > /tmp/$$.sortcnt
    cat /tmp/$$.sortnums | sed -e "s/^/${1}|/" | sed -e "s/$/|4|True|${BKT}/" > /tmp/$$.fptemp
    paste -d"|" /tmp/$$.fptemp /tmp/$$.sortcnt >> /tmp/${1}.newcntfile
  fi
  BKT=`expr $BKT \+ 1`
done
#grep 4\|True /tmp/${1}.newcntfile | cut -f6 -d"|" | paste -sd+ - | bc
#RCNT=`cat /tmp/$$.rcnt | paste -sd+ - | bc`
#RTOT=`cat /tmp/$$.rtcnt | paste -sd+ - | bc`
#echo "$1:$RCNT:RESTTAGS"
#echo "$1:$RTOT:RESTTOT"
rm -f /tmp/$$.*
fi
#
#
# FONES and FP
if
  test "$SKIPFNFP" != "1"
then

grep -e :DELEGATOR: $EDGEFILE | grep -v "^#" > /tmp/$$.SERVERS
for SERVER in `cat /tmp/$$.SERVERS`
do
  TDRIP=`echo $SERVER | cut -f5 -d":"`
  zgrep FONES $DIR/archive/tdrmon@${TDRIP}/${1}*.gz | cut -f4-26 -d"|" | grep TRUE$ >> /tmp/$$.fnpnbcallids
  grep -c SpamRule /tmp/$$.fnpnbcallids >> /tmp/$$.fones_cnt
  #cat /tmp/$$.fnpnbcallids | wc -l >> /tmp/$$.tfones_cnt
  grep SpamRule /tmp/$$.fnpnbcallids >> /tmp/$$.allspamfn_callids
  rm /tmp/$$.fnpnbcallids
  zgrep FINGER $DIR/archive/tdrmon@${TDRIP}/${1}*.gz | cut -f4-26 -d"|" | grep TRUE$ >> /tmp/$$.fppnbcallids
  zgrep \|DELEGATOR\| $DIR/archive/tdrmon@${TDRIP}/${1}*.gz | cut -f6,16 -d"|" >> /tmp/$$.del_callids
  #grep -c SpamRule /tmp/$$.fppnbcallids >> /tmp/$$.fp_cnt
  #cat /tmp/$$.fppnbcallids | wc -l >> /tmp/$$.tfp_cnt
  grep SpamRule /tmp/$$.fppnbcallids >> /tmp/$$.allspamfp_callids
  #wc -l /tmp/$$.fppnbcallids
  #wc -l /tmp/$$.allspamfp_callids
  rm /tmp/$$.fppnbcallids
done
### FP
#only use FP IDS chosen by DELEGATOR
cat /tmp/$$.allspamfp_callids | cut -f1 -d"|" | sort -u > /tmp/$$.sortfp_callids
grep \|6[05]$ /tmp/$$.del_callids | cut -f1 -d"|" | sort > /tmp/$$.sortdel_callids
diff /tmp/$$.sortdel_callids /tmp/$$.sortfp_callids | grep "^>" | cut -c3- > /tmp/$$.notfp_per_del
NOTPERDEL=` cat /tmp/$$.notfp_per_del | wc -l`
if
  test $NOTPERDEL -gt 0
then
  while
    read NOTPERDELID
  do
    grep -v "^${NOTPERDELID}" /tmp/$$.allspamfp_callids > /tmp/$$.tmpcallids
    mv /tmp/$$.tmpcallids /tmp/$$.allspamfp_callids
  done < /tmp/$$.notfp_per_del
fi
#wc -l /tmp/$$.allspamfp_callids
cp /tmp/$$.allspamfp_callids /tmp/spamfp
BKT=0
while 
  test $BKT -lt 16
do
  grep SpamRule\|$BKT\| /tmp/$$.allspamfp_callids | cut -f4 -d"|" | sort | uniq -c > /tmp/$$.fn${BKT}
  BKTCNT=`cat /tmp/$$.fn${BKT} | wc -l`
  if
    test $BKTCNT -gt 0
  then
    cut -c1-7 /tmp/$$.fn${BKT} | sed -e "s/ //g" > /tmp/$$.cnts
    cut -c9- /tmp/$$.fn${BKT} > /tmp/$$.nums
    paste -d"|" /tmp/$$.nums /tmp/$$.cnts > /tmp/$$.newfile
    sort /tmp/$$.newfile > /tmp/$$.sortfile
    cut -f1 -d"|" /tmp/$$.sortfile > /tmp/$$.sortnums
    cut -f2 -d"|" /tmp/$$.sortfile > /tmp/$$.sortcnt
    cat /tmp/$$.sortnums | sed -e "s/^/${1}|/" | sed -e "s/$/|3|True|${BKT}/" > /tmp/$$.fptemp
    paste -d"|" /tmp/$$.fptemp /tmp/$$.sortcnt >> /tmp/${1}.newcntfile
  fi
  BKT=`expr $BKT \+ 1`
done
#grep 3\|True /tmp/${1}.newcntfile | cut -f6 -d"|" | paste -sd+ - | bc
#
#
### FONES
#wc -l /tmp/$$.allspamfn_callids
cat /tmp/$$.allspamfn_callids | cut -f1 -d"|" | sort -u > /tmp/$$.sortfn_callids
grep \|[247]0$ /tmp/$$.del_callids | cut -f1 -d"|" | sort > /tmp/$$.sortdel_callids
diff /tmp/$$.sortdel_callids /tmp/$$.sortfn_callids | grep "^>" | cut -c3- > /tmp/$$.notfn_per_del
NOTPERDEL=` cat /tmp/$$.notfn_per_del | wc -l`
#echo $NOTPERDEL
if
  test $NOTPERDEL -gt 0
then
  #wc -l /tmp/$$.notfn_per_del
  while
    read NOTPERDELID
  do
    #grep $NOTPERDELID /tmp/$$.del_callids >> /tmp/$$.checkthis
    grep -v "^${NOTPERDELID}" /tmp/$$.allspamfp_callids > /tmp/$$.tmpcallids
    mv /tmp/$$.tmpcallids /tmp/$$.allspamfp_callids
  done < /tmp/$$.notfn_per_del
  #cut -f2 -d"|" /tmp/$$.checkthis | sort | uniq -c
fi
BKT=0
while 
  test $BKT -lt 16
do
  grep SpamRule\|$BKT\| /tmp/$$.allspamfn_callids | cut -f3 -d"|" | sort | uniq -c > /tmp/$$.fn${BKT}
  BKTCNT=`cat /tmp/$$.fn${BKT} | wc -l`
  if
    test $BKTCNT -gt 0
  then
    cut -c1-7 /tmp/$$.fn${BKT} | sed -e "s/ //g" > /tmp/$$.cnts
    cut -c9- /tmp/$$.fn${BKT} > /tmp/$$.nums
    paste -d"|" /tmp/$$.nums /tmp/$$.cnts > /tmp/$$.newfile
    sort /tmp/$$.newfile > /tmp/$$.sortfile
    cut -f1 -d"|" /tmp/$$.sortfile > /tmp/$$.sortnums
    cut -f2 -d"|" /tmp/$$.sortfile > /tmp/$$.sortcnt
    cat /tmp/$$.sortnums | sed -e "s/^/${1}|/" | sed -e "s/$/|2|True|${BKT}/" > /tmp/$$.fptemp
    paste -d"|" /tmp/$$.fptemp /tmp/$$.sortcnt >> /tmp/${1}.newcntfile
  fi
  BKT=`expr $BKT \+ 1`
done
#grep 2\|True /tmp/${1}.newcntfile | cut -f6 -d"|" | paste -sd+ - | bc
#DTOT=`cat /tmp/$$.tfones_cnt | paste -sd+ - | bc`
#wc -l /tmp/$$.allspamfn_callids
#FNCNT=`cat /tmp/$$.fones_cnt | paste -sd+ - | bc`
#echo "$1:$FNCNT:FONESTAGS"
#echo "$1:$DTOT:DELTOT not tagged is cnam anyway"
#wc -l /tmp/$$.allspamfp_callids
#FPTOT=`cat /tmp/$$.tfp_cnt | paste -sd+ - | bc`
#FPCNT=`cat /tmp/$$.fp_cnt | paste -sd+ - | bc`
#echo "$1:$FPCNT:FPTAGS"
#echo "$1:$FPTOT:FPTOT not tagged is cnam anyway"
rm -f /tmp/$$.*
fi
