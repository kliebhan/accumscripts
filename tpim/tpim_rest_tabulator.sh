for SERVER in `echo DLLSPC02 DLLSPC03 ELGSPC02 ELGSPC03 NSVSPC01 NSVSPC02`
do
  zgrep -i httpresponsecount /home/tdr/tpim/$SERVER/*20180[12]*.log.gz > /tmp/$SERVER.rc.txt
  zgrep -i httpresultresponsecount /home/tdr/tpim/$SERVER/*20180[12]*.log.gz | grep -v \|200-OK\| > /tmp/$SERVER.sr.txt
done
echo "SERVER:DAY:DATA"
for SERVER in `echo DLLSPC02 DLLSPC03 ELGSPC02 ELGSPC03 NSVSPC01 NSVSPC02`
do
 for DY in `echo 28 29 30 31 01 02`
 do
  grep "${SERVER}.20180[12]${DY}" /tmp/$SERVER.rc.txt | cut -f6 -d"|" > /tmp/$$.daycnt${DY}
  grep "${SERVER}.20180[12]${DY}" /tmp/$SERVER.sr.txt | cut -f6 -d"|" > /tmp/$$.sr${DY}
  DAYCNT=`awk '{s+=$1} END {printf "%.0f", s}' /tmp/$$.daycnt${DY}`
  ERRCNT=`awk '{s+=$1} END {printf "%.0f", s}' /tmp/$$.sr${DY}`
  echo $SERVER:$DY:${DAYCNT} >> /tmp/servdycnt.txt
  echo $SERVER:$DY:${DAYCNT}
  #echo $SERVER:$DY:${ERRCNT} 
  #echo $SERVER:$DY:ErrorRate:`echo "scale=6; $ERRCNT / $DAYCNT" | bc -l`
 done
 #echo $SERVER:transactions-peakday:`cut -f3 -d":" /tmp/servdycnt.txt | sort -n | tail -1`
 rm -f /tmp/servdycnt.txt
 rm -f /tmp/serverrcnt.txt
 rm -f /tmp/$$.sr${DY}
 rm -f /tmp/$$.daycnt*
done
echo "DAY:non-200-OK %"
for DY in `echo 28 29 30 31 01 02`
do
  DYTOT=`grep "20180[12]${DY}" /tmp/*.rc.txt | cut -f6 -d"|" | paste -sd+ - | bc`
  ERTOT=`grep "20180[12]${DY}" /tmp/*.sr.txt | cut -f6 -d"|" | paste -sd+ - | bc`
  echo $DY:ErrorRate:`echo "scale=6; $ERTOT / $DYTOT" | bc -l`
done
echo "SERVER:DAY:HR:4XX DATA"
for SERVER in `echo DLLSPC02 DLLSPC03 ELGSPC02 ELGSPC03 NSVSPC01 NSVSPC02`
do
 #for DY in `echo 28 29 30 31 01 02`
 for DY in `echo 01`
 do
  for HR in `echo 00 01 02 03 04 05 06 07 08 09 10 11 12 13 14 15 16 17 18 19 20 21 22 23`
  do
    grep "${SERVER}.20180[12]${DY}T${HR}" /tmp/$SERVER.rc.txt | cut -f6 -d"|" > /tmp/$$.hrcnt${DY}${HR} 
    grep "${SERVER}.20180[12]${DY}T${HR}" /tmp/$SERVER.sr.txt | cut -f6 -d"|" > /tmp/$$.hrsr${DY}${HR}
    HRCNT=`awk '{s+=$1} END {printf "%.0f", s}' /tmp/$$.hrcnt${DY}${HR}`
    echo $HRCNT >> /tmp/janhrcnts.txt
    ERRCNT=`awk '{s+=$1} END {printf "%.0f", s}' /tmp/$$.hrsr${DY}${HR}`
    if
      test $HRCNT -gt 0
    then
      echo $SERVER:$DY:$HR:ErrorRate:`echo "scale=6; $ERRCNT / $HRCNT" | bc -l`
    fi
  done
 done
 echo $SERVER:peakhr:`cat /tmp/janhrcnts.txt | sort -n | tail -1`
 rm /tmp/janhrcnts.txt
done
rm /tmp/$$.*
