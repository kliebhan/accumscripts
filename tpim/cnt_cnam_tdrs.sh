for IP in `grep CNAM ../edge_ip_list | cut -f5 -d":"`
do
  DIR=/pstar/tdr/archive/tdrmon@${IP}
  MN=02
  #for DY in `echo 01 02 03 04 05 06 07 08 09 10 11 12 13 14 15 16 17 18 19 20 21 22 23 24 25`
  for DY in `echo 20`
  do
    zgrep -c -i "scam likely" ${DIR}/2018${MN}${DY}*.gz >> /tmp/$$.cnt
 done
done
cat /tmp/$$.cnt | cut -f2 -d":" | paste -sd+ - | bc
rm /tmp/$$.cnt
exit
 TOTCNT=`cat /tmp/$$.daycnt${DY} | paste -sd+ - | bc`
 TAGCNT=`cat /tmp/$$.tagcnt${DY} | paste -sd+ - | bc`
 echo day:$DY:cnt:$TOTCNT
 echo day:$DY:tag:$TAGCNT
 echo day:$DY:TAGRate:`echo "scale=6; ($TAGCNT / $TOTCNT) *100" | bc -l`
done
rm $$.daycnt*
rm $$.tagcnt*
