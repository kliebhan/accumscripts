#for SERVER in `echo LIVSPC01 DLLSPC04`
#do
#  zgrep -i cnamrequestcount /home/tdr/tpim/$SERVER/*201801*.log.gz > /tmp/$SERVER.cnamreq.txt
#  zgrep -i cnamtagcount /home/tdr/tpim/$SERVER/*201801*.log.gz > /tmp/$SERVER.tagtxt
#done
rm -f $$.daycnt*
for DY in `echo 01 02 03 04 05 06 07 08 09 10 11 12 13 14 15 16 17 18 19 20 21 22 23 24 25`
do
 for SERVER in `echo LIVSPC01 DLLSPC04`
 do
  grep "${SERVER}.201801${DY}" /tmp/$SERVER.cnamreq.txt | cut -f6 -d"|" >> /tmp/$$.daycnt${DY}
  grep "${SERVER}.201801${DY}" /tmp/$SERVER.tagtxt | cut -f6 -d"|" >> /tmp/$$.tagcnt${DY}
 done
 #echo day:$DY:cnt:`awk '{s+=$1} END {printf "%.0f", s}' /tmp/$$.daycnt${DY}`
 TOTCNT=`cat /tmp/$$.daycnt${DY} | paste -sd+ - | bc`
 TAGCNT=`cat /tmp/$$.tagcnt${DY} | paste -sd+ - | bc`
 echo day:$DY:cnt:$TOTCNT
 echo day:$DY:tag:$TAGCNT
 echo day:$DY:TAGRate:`echo "scale=6; ($TAGCNT / $TOTCNT) *100" | bc -l`
done
rm $$.daycnt*
rm $$.tagcnt*
