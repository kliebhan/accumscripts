#for SERVER in `echo DLLSPC01 DLLSPC05 ELGSPC01 ELGSPC04 POLSPC01 CHRSPC01 CHRSPC02 WNRSPC01 SYOSPC01 IRVSPC01 IRVSPC02 LIVSPC02 AURSPC01`
#do
#  zgrep -i enumquerycount /home/tdr/tpim/$SERVER/*201801*.log.gz > /tmp/$SERVER.jan419.txt
#done
#for SERVER in `echo DLLSPC01 DLLSPC05 ELGSPC01 POLSPC01 CHRSPC01 CHRSPC02 WNRSPC01 SYOSPC01 IRVSPC01 IRVSPC02 LIVSPC02 AURSPC01`
#do
# for DY in `echo 04 05 06 07 08 09 10 11 12 13 14 15 16 17 18 19`
# do
#  grep "${SERVER}.201801${DY}" /tmp/$SERVER.jan419.txt | cut -f6 -d"|" > /tmp/$$.daycnt${DY}
#  echo day:$DY:`awk '{s+=$1} END {printf "%.0f", s}' /tmp/$$.daycnt${DY}` >> /tmp/jandaycnts.txt
# done
# echo $SERVER:peakday:`cut -f3 -d":" /tmp/jandaycnts.txt | sort -n | tail -1`
# rm /tmp/jandaycnts.txt
#done
for SERVER in `echo DLLSPC01 DLLSPC05 ELGSPC01 POLSPC01 CHRSPC01 CHRSPC02 WNRSPC01 SYOSPC01 IRVSPC01 IRVSPC02 LIVSPC02 AURSPC01`
do
 for DY in `echo 04 05 06 07 08 09 10 11 12 13 14 15 16 17 18 19`
 do
  for HR in `echo 00 01 02 03 04 05 06 07 08 09 10 11 12 13 14 15 16 17 18 19 20 21 22 23`
  do
    grep "${SERVER}.201801${DY}T${HR}" /tmp/$SERVER.jan419.txt | cut -f6 -d"|" > /tmp/$$.hrcnt${DY}${HR} 
    awk '{s+=$1} END {printf "%.0f", s}' /tmp/$$.hrcnt${DY}${HR} >> /tmp/janhrcnts.txt
    echo >> /tmp/janhrcnts.txt
  done
 done
 echo $SERVER:peakhr:`cat /tmp/janhrcnts.txt | sort -n | tail -1`
 rm /tmp/janhrcnts.txt
done
