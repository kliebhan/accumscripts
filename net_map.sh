set -x
grep -e :DELEGATOR: $HOME/dfkbin/edge_ip_list | grep -v "^#" > /tmp/$$.SERVERS
EDT=`date +%s`
LASTHR=`expr $EDT \- 3600`
DT=`date -d@${LASTHR} -u +%Y%m%d%H`
for SERVER in `cat /tmp/$$.SERVERS`
do
  TDRIP=`echo $SERVER | cut -f5 -d":"`
  zgrep -e \|DELEGATOR\| $HOME/archive/tdrmon@${TDRIP}/${DT}*.gz  | cut -f3,11 -d"|" | sort -u >> /tmp/netmap${DT}.txt
done
LHR=`date -d@${LASTHR} -u +%H`
if
   test "$LHR" = "10"
then
   YDT=`expr $EDT \- 86400`
   YDY=`date -d@${YDT} +%Y%m%d`
   diff /tmp/netmap${YDY}2*.txt /tmp/netmap${DT}.txt > /tmp/last_diff.txt
else
   TDY=`date -d@${EDT} +%Y%m%d`
   diff /tmp/netmap${TDY}1*.txt /tmp/netmap${DT}.txt > /tmp/last_diff.txt
fi
DIFFL=`cat /tmp/last_diff.txt | wc -l`
if
  test $DIFFL -gt 0
then
  cat /tmp/last_diff.txt
fi
