OUTFILE=/tmp/revChkTas.out
#OUTFILE=./revChkTas.out
EMAILDIR=/pstar/tmoreports/outbound/enum_heartbeat_failure
TASFILE=/pstar/tdr/dfkbin/TASNAMES
REMCHKF=/pstar/tdr/dfkbin/revChkTas.sh
SENDFAIL=0
DOREMOTE=1
if
  test $DOREMOTE -ne 0
then
  scp $TASFILE dkliebhan@10.250.22.15:/tmp 1>/dev/null 2>&1
  scp $REMCHKF dkliebhan@10.250.22.15:/tmp 1>/dev/null 2>&1
  ssh dkliebhan@10.250.22.15 "/tmp/revChkTas.sh" 1>/dev/null 2>&1
  scp dkliebhan@10.250.22.15:$OUTFILE $OUTFILE 1>/dev/null 2>&1
else
  ./revChkTas.sh
fi
if
  test -f "$OUTFILE"
then
  DS=`date +%s`
  TS=`head -1 $OUTFILE`
  DS=`echo "$DS - 30" | bc`
  if
    test $TS -lt $DS
  then
    SENDFAIL=99
    echo "Check did not run successfully"
  else
    FAILS=`grep -v NVTAS001 $OUTFILE | \
           grep -v NVTAS002 | \
           grep -v NVTAS003 | \
           grep -v HSTTAS03 | \
           grep -v HSTTAS04 | \
	   grep -c ":$"`
    if
      test $FAILS -gt 0
    then
      SENDFAIL=1
    fi
    FWDCHK=0
    for FNDTAS in `grep ":" $OUTFILE | cut -f1 -d":"`
    do
      HAVETASNAME=`grep -c $FNDTAS $TASFILE`
      if
        test $HAVETASNAME -eq 0
      then
        echo "we have an unknown TAS:$FNDTAS" >> /tmp/$$.notasname
        FWDCHK=2
      fi
    done
    SENDFAIL=`expr $SENDFAIL \+ $FWDCHK`
  fi
else
  SENDFAIL=99
fi
D=`date`
case $SENDFAIL in
	0)
	   echo "all is well"
	   ;;
        1)
	   echo "reverse check failure"
           grep -v NVTAS001 $OUTFILE | \
             grep -v NVTAS002 | \
             grep -v NVTAS003 | \
             grep -v HSTTAS03 | \
             grep -v HSTTAS04 | \
	     grep ":$" > /tmp/$$.failingTAS
  	   echo "The following TAS names have no POST queries in the 15 minute interval near date $D" >> /tmp/$$.emailfailingTAS
  	   cat /tmp/$$.failingTAS >>/tmp/$$.emailfailingTAS
 	   cat /tmp/$$.emailfailingTAS
	   ;;
        2)
	   echo "forward check failure"
  	   echo "The following TAS names have transaction but are not in the list of known TASs" >> /tmp/$$.emailfailingTAS
  	   cat /tmp/$$.notasname >>/tmp/$$.emailfailingTAS
 	   cat /tmp/$$.notasname
	   ;;
        3)
	   echo "rev and forward check failure"
           grep -v NVTTAS001 $OUTFILE | \
             grep -v NVTTAS002 | \
             grep -v NVTTAS003 | \
             grep -v HSTTAS003 | \
             grep -v HSTTAS004 | \
	     grep ":$" > /tmp/$$.failingTAS
  	   echo "The following TAS names have no POST queries in the 15 minute interval near date $D" >> /tmp/$$.emailfailingTAS
  	   cat /tmp/$$.failingTAS >>/tmp/$$.emailfailingTAS
 	   cat /tmp/$$.emailfailingTAS
  	   echo "The following TAS names have transaction but are not in the list of known TASs" >> /tmp/$$.emailfailingTAS
  	   cat /tmp/$$.notasname >>/tmp/$$.emailfailingTAS
 	   cat /tmp/$$.notasname
	   ;;
        99)
	   echo "did not get a returned file from TPIM"
	   ;;
        *)
	   echo "someting very bad happened"
           ;;
esac
if
   test $SENDFAIL -gt 0
then
 cp /tmp/$$.emailfailingTAS $EMAILDIR/"TAS FAILURES at $D"
fi
rm -f /tmp/$$.*
