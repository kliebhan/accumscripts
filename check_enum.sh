LOG=$HOME/logs/enumstatus.log
EMAILDIR=/pstar/tmoreports/outbound/enum_heartbeat_failure
FC=0
grep :ENUM: $HOME/dfkbin/edge_ip_list | grep -v "^#" > /tmp/$$.SERVERS
SC=`cat /tmp/$$.SERVERS | wc -l`
for SERVER in `cat /tmp/$$.SERVERS`
do
  PROTIP=`echo $SERVER | cut -f2 -d":"`
  NAME=`echo $SERVER | cut -f4 -d":"`
  R=`dig @${PROTIP} -t naptr 7.2.0.0.8.2.2.4.1.2.1.$.5.1.8.9.9.3.9.6.0.2.1.isbc.t-mobile.e164.arpa | grep TYPE65282 | wc -l`
  if 
    test "$R" != "1"
  then
    D=`date -u +%s`
    echo $D:$NAME:ENUMfailed >> $LOG
    FC=`expr $FC \+ 1`
  fi
done
if
  test $FC -eq 0
then
  D=`date -u +%s`
  echo $D:$SC:ENUMPASSED >> $LOG
fi
rm -f /tmp/$$.*
